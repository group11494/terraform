resource "aws_vpc" "vpc1" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_hostnames = true
  tags = {
    Name = "vpc1"
  }
}

resource "aws_subnet" "publicsubnet" {
  vpc_id            = aws_vpc.vpc1.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "ap-south-1a"
  map_public_ip_on_launch = "true"
  tags = {
    Name = "public-subnet1"
  }
}

resource "aws_subnet" "publicsubnet2" {
  vpc_id            = aws_vpc.vpc1.id
  cidr_block        = "10.0.0.0/24"
  availability_zone = "ap-south-1b"
  map_public_ip_on_launch = "true"
  tags = {
    Name = "public-subnet2"
  }
}
resource "aws_route_table" "publicrt" {
  vpc_id = aws_vpc.vpc1.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
  tags = {
    Name = "pulic-rt1"
  }
}
resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.publicsubnet.id
  route_table_id = aws_route_table.publicrt.id
}
resource "aws_route_table_association" "b" {
  subnet_id      = aws_subnet.publicsubnet2.id
  route_table_id = aws_route_table.publicrt.id
}
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.vpc1.id

  tags = {
    Name = "igw-1"
  }
}

// ec2.tf
resource "aws_instance" "ec2" {
  ami                    = var.ami_id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.keypair1.key_name
  vpc_security_group_ids = ["${aws_security_group.allow_tls.id}"]
  user_data              = file("${path.module}/userdatascript.sh")
  subnet_id              = aws_subnet.publicsubnet.id
  tags = {
    Name = "ec2-1"
  }
  associate_public_ip_address = false
}
//key-pair
resource "aws_key_pair" "keypair1" {
  key_name   = "kp1"
  public_key = file("${path.module}/rsakeypair1.pub")
}
//out.tf
output "name" {
  value = "Hii, your name is ${var.name}"
  // sensitive   = true
  // description = "description"
  //  depends_on  = []
}
output "keypairid" {
  value = aws_key_pair.keypair1.key_pair_id

}

output "securitygroupid" {
  value = aws_security_group.allow_tls.id
}
//provider.tf
# terraform {
#   #required_version = ">= 0.12" --Terraform v1.3.6
#  backend "s3" {
#    bucket="dryiceinfra-terraform-states"
#    key="default/env-rsc/terraform.tfstate"
#    region="ap-south-1"
#    # dynamodb_table= "dryice-environment-states-locks"
#    # encrypt = true

#    }

# }


provider "aws" {
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}
//sg.tf
resource "aws_security_group" "allow_tls" {
  name        = "sg1"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.vpc1.id

  dynamic "ingress" {
    for_each = var.ports
    iterator = port
    content {
      description = "TLS from VPC"
      from_port   = port.value
      to_port     = port.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}
//.tfvars
region        = "ap-south-1"
access_key    = "AKIAVUP7UYXUMF3I2FYQ"
secret_key    = "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"
ports         = [22, 443, 80]
name          = "Rinki Rathore.."
ami_id        = "ami-01a4f99c4ac11b03c"
instance_type = "t2.micro"
//userdatascript.sh
#!/bin/bash
sudo su
yum update -y && yum install httpd -y
systemctl start httpd & systemctl enable httpd
echo "hello rinki" > /var/www/html/index.html
//var.tf
variable "region" {
  type        = string
  default     = "ap-south-1"
  description = "region for terraform infra creation"
}
variable "access_key" {
  type = string
}
variable "secret_key" {
  type = string
}
variable "name" {
  type = string
}
variable "ports" {
  type = list(any)
}
variable "ami_id" {
  type = string
}
variable "instance_type" {
  type = string
}
